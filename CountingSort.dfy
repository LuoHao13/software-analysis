// Importing the sets module which is necessary for Multiset
import opened sets

// Counting Sort implementation in Dafny
method CountingSort(arr: array<int>) returns (sortedArr: array<int>)
    requires arr != null && arr.Length > 0 // Input array must not be null and must have at least one element
    ensures sortedArr != null && sortedArr.Length == arr.Length // Sorted array should match the input array in size
    ensures forall i, j :: 0 <= i < j < sortedArr.Length ==> sortedArr[i] <= sortedArr[j] // Sorted array must be in non-decreasing order
    ensures Multiset(sortedArr[..]) == Multiset(arr[..]) // The sorted array is a permutation of the input array
{
    var maxVal := arr[0];
    for i := 1 to arr.Length - 1
        invariant 1 <= i <= arr.Length
        invariant forall j :: 0 <= j < i ==> arr[j] <= maxVal
    {
        if arr[i] > maxVal {
            maxVal := arr[i];
        }
    }

    // Initialize the count array with zeros
    var countArr := new array<int>(maxVal + 1, fill := 0);
    for i := 0 to arr.Length - 1
        invariant 0 <= i <= arr.Length
        invariant forall k :: 0 <= k < countArr.Length ==> countArr[k] == |{j | 0 <= j < i && arr[j] == k}|
    {
        countArr[arr[i]] := countArr[arr[i]] + 1;
    }

    // Initialize the sorted array
    sortedArr := new array<int>(arr.Length, fill := 0);
    var index := 0;
    for i := 0 to countArr.Length - 1
        invariant 0 <= i <= countArr.Length
        invariant 0 <= index <= sortedArr.Length
        invariant forall k :: 0 <= k < i ==> countArr[k] == |{j | 0 <= j < sortedArr.Length && sortedArr[j] == k}|
        invariant forall l :: i <= l < countArr.Length ==> countArr[l] == |{j | 0 <= j < arr.Length && arr[j] == l}|
    {
        var count := countArr[i];
        while count > 0
            decreases count
        {
            sortedArr[index] := i;
            index := index + 1;
            count := count - 1;
        }
    }
}
